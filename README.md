# nyaa
cli tool for streaming and downloading torrents from [nyaa.si](https://nyaa.si), heavily inspired by [notflix](https://github.com/Bugswriter/notflix)

## Dependencies

* [pmenu](https://github.com/sgtpep/pmenu) - Dynamic terminal-based menu
* [webtorrent](https://github.com/webtorrent/webtorrent-cli) - Tool to download torrents
* [aria2](https://aria2.github.io/) - Lightweight alternative for webtorrent

## Usage

```sh
$ nyaa                  # will prompt for query
$ nyaa your query here
$ nyaa fate apocrypha   # an example
```

### Flags

- d) download torrent to $NYAA_DIR using aria2
- p) create a playlist of all files in the torrent
- x) query [sukebei.nyaa.si](https://sukebei.nyaa.si)

### Config

The program sources the file at $XDG_CONFIG_HOME/nyaa/config or $HOME/.config/nyaa/config
Currently, there are only 2 environmental variables available

- NYAA_DIR -> directory to which the torrents will be downloaded
- NYAA_PLAYER_ARGS -> arguments passed to mpv

Example of the config file:

```sh
NYAA_DIR=$HOME/Torrents
NYAA_PLAYER_ARGS="--slang=eng --alang=jpn"
```

## Installation

```sh
$ sudo curl -sL "https://gitlab.com/yaemiku/nyaa/-/raw/main/nyaa" -o /usr/local/bin/nyaa
$ sudo chmod +x /usr/local/bin/nyaa
```

To upgrade, you only need to run the first command

